import React, {Component} from 'react';
import styled from 'styled-components';

const sideBarWidth =  `width: 300px`;

const SideBarStyle = styled.div`
  background: black;
  ${sideBarWidth}
  height: 100%;
`;

class SideBar extends Component {
  render() {
    return <SideBarStyle> </SideBarStyle>
  }
};

export default SideBar;