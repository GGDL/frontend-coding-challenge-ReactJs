import React, {Component} from 'react';
import styled from 'styled-components';
import Card from './../card';

const ContentDiv = styled.div`
  background: red;
  height: 100%;
  padding-left: 30px;
  display: flex;
`;

class CardsContainer extends Component {
  render() {
    return(
    <ContentDiv>
        <Card/>
        <Card/>
        <Card/>
    </ContentDiv>)
  }
};

export default CardsContainer;
