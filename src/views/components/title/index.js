import React, {Component} from 'react';
import styled from 'styled-components';

const TitleStyle = styled.div`
  padding: 30px;
`;

const Content = styled.div`
  background: yellow;
  height: 100%;
`;

class Title extends Component {
  render() {
    return <TitleStyle>LANGUAGES & TECNOLOGIES</TitleStyle>
  }
};

export default Title;