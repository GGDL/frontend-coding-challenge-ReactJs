import React, {Component} from 'react';
import styled from 'styled-components';
import Card from './../card';
import Title from './../title';
import CardsContainer from './../cards-container';

const ContentDiv = styled.div`
  background: yellow;
  width: calc(100% - 300px);
  height: 100%;
  display: flex;
  flex-direction: column;
`;

class Content extends Component {
  render() {
    return(
    <ContentDiv>
      <Title></Title>
      <CardsContainer>
      </CardsContainer>
    </ContentDiv>)
  }
};

export default Content;
